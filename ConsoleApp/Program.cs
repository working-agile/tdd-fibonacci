﻿﻿using System;
using FibonacciLibrary;

class Program
{
    static void Main(string[] args)
    {
        int row = 0;

        do
        {
            if (row == 0 || row >= 25)
                ResetConsole();


            Console.WriteLine("Enter a value: \n");

            int value = Convert.ToInt32(Console.ReadLine());

            if (string.IsNullOrEmpty(value.ToString())) break;

            Console.WriteLine($"\n Input: {value} {"Fibonacci result: ",30}: " +
                          $"{Fibonacci.calculate(value)}\n");

            row += 3;
        } while (true);
        return;

        // Declare a ResetConsole local method
        void ResetConsole()
        {
            if (row > 0)
            {
                Console.WriteLine("Press any key to continue...");
                Console.ReadKey();
            }
            Console.Clear();
            Console.WriteLine("\nPress <Enter> only to exit\n");
            row = 3;
        }
    }
}